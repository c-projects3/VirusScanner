#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dirent.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <crtdbg.h>

#define SLASH "\\"
#define SLASH_LEN strlen(SLASH)

#define LOG_FILE_NAME "AntiVirusLog.txt"

#define READ_MODE "rb"

#define FIRST_20_P " (first 20%)"
#define FIRST_20_P_LEN strlen(FIRST_20_P)
#define LAST_20_P " (last 20%)"
#define LAST_20_P_LEN strlen(LAST_20_P)

#define INFECTED_VIRUS "  Infected!"
#define INFECTED_VIRUS_LEN strlen(INFECTED_VIRUS)

#define CLEAN "  Clean"
#define CLEAN_LEN strlen(CLEAN)

#define NEW_LINE '\n'

#define NORMAL_MODE  "Normal"
#define QUICK_SCAN "Quick Scan"

#define F_LOG_DESCRIBE_1 "Anti-virus began! Welcome!\n\nFolder to scan :\n"
#define F_LOG_DESCRIBE_2 "\nVirus signature :\n"
#define F_LOG_DESCRIBE_3 "\n\nScanning option :\n"
#define F_LOG_DESCRIBE_4 "\n\nResults :\n"

#define SKIP !((strcmp(ent->d_name, ".") == 0) || (strcmp(ent->d_name, "..") == 0))
#define SKIP_LOG_FILE (strcmp(ent->d_name, LOG_FILE_NAME))
#define LOG_FILE_LEN strlen(LOG_FILE_NAME)

#define FIFTH 0.2
#define NORMAL '0'

#define PARAMS 3
#define PATH_INDEX 1
#define VIRUS_INDEX 2

#define MAX 200

#define ERROR_STATUS 1
#define THE_FILE_IS_OK 0
#define FIND_THE_VIRUS 1

#define ERROR 0
#define SUCCES 1

#define FAILED 0
#define SECESS 1

#define EQUAL 0

#define KEEP_SEARCHING 1
#define STOP_SEARCHING 0

#define FOUND 1


void wellcome_message(char* directory_name, char* virus_name);
void is_directory(const char* path);
void valid_number_of_arguments(int n);
void get_scan_option(char* ch);


void general_scan(char* directory_path, char* virus_file, char mode, FILE* flog);
int search_virus(FILE* file, FILE* virus, long bytes);
void file_contains_virus_fast(char* virus_file_name, char* file_name_to_check, FILE* flog);
void file_contains_virus(char* virus_file_name, char* file_name_to_check, FILE* flog);

long get_file_length(FILE* file);
void write_f_log(FILE* flog, char* dir_name, char* f_name, char* mode);
char* get_mode(char mode);
void open_file(FILE** f, char* path, char* mode);
int valid_file(FILE* f); 
void open_dir(DIR** d, char* path);


int main(int argc, char* argv[])
{
	FILE* flog = NULL;
	char option = ' ';
	char full_path[MAX] = { 0 };

	valid_number_of_arguments(argc);
	is_directory(argv[PATH_INDEX]);

	wellcome_message(argv[PATH_INDEX], argv[VIRUS_INDEX]);
	get_scan_option(&option);

	// create a full path to the log file
	strncat(full_path, argv[PATH_INDEX], strlen(argv[PATH_INDEX]));
	strncat(full_path, SLASH, SLASH_LEN);
	strncat(full_path, LOG_FILE_NAME, LOG_FILE_LEN);

	// open the log and put the template message
	open_file(&flog, full_path, "w");
	write_f_log(flog, argv[PATH_INDEX], argv[VIRUS_INDEX], get_mode(option));
	

	general_scan(argv[PATH_INDEX], argv[VIRUS_INDEX], option, flog);
	
	printf("Scan Completed!\n");
	printf("See log path for rezult: %s\n", full_path);
	fclose(flog);
	
	getchar();
	getchar();
	return 0;
}


/*  The function get the folder and the virus signature, and run thuogh the folder to scan him
	input: folder path, virus path, ptr to log file
	output: none*/
void general_scan(char* directory_path, char* virus_file, char mode, FILE* flog)
{
	DIR* d = NULL;
	struct dirent* ent = NULL;
	char current_file_path[MAX] = { 0 };

	open_dir(&d, directory_path);

	while ((ent = readdir(d)) != NULL)
	{
		// skip the "." & ".." & folders & log file
		if (SKIP && (ent->d_type != DT_DIR) && SKIP_LOG_FILE)
		{
			// create the current file path
			memset(current_file_path, 0, strlen(current_file_path));
			strncat(current_file_path, directory_path, strlen(directory_path));
			strncat(current_file_path, SLASH, SLASH_LEN);
			strncat(current_file_path, ent->d_name, strlen(ent->d_name));
			switch (mode)
			{
			case NORMAL:
				file_contains_virus(virus_file, current_file_path, flog);
				break;

			default:
				file_contains_virus_fast(virus_file, current_file_path, flog);
				break;
			}
		}
	}
	closedir(d);
}


/*  function get virus signature, and file, and check heuristics if the file contains the virus
	input: virus signature file path, file to check path
	output: none*/
void file_contains_virus_fast(char* virus_file_name, char* file_name_to_check, FILE* flog)
{
	long twenty_precent = 0;
	long total_file_length = 0;
	FILE* file = NULL;
	FILE* virus = NULL;
	int find = 0;
	
	open_file(&file, file_name_to_check, READ_MODE);
	open_file(&virus, virus_file_name, READ_MODE);

	if (!valid_file(virus))
	{
		fclose(file);
		exit(ERROR_STATUS);
	}
	total_file_length = get_file_length(file);
	twenty_precent = FIFTH * total_file_length;

	// search the virus, sent the 20 % as how match to scan
	if(search_virus(file, virus, twenty_precent))
	{
		find = FOUND;
		strncat(file_name_to_check, INFECTED_VIRUS, INFECTED_VIRUS_LEN);
		strncat(file_name_to_check, FIRST_20_P, FIRST_20_P_LEN);
	}
	
	// put the file cursor in 80 % and sent the total len (from 80% to 100%...)
	fseek(file, - twenty_precent, SEEK_END);
	if (search_virus(file, virus, total_file_length) && !find)
	{
		find = FOUND;
		strncat(file_name_to_check, INFECTED_VIRUS, INFECTED_VIRUS_LEN);
		strncat(file_name_to_check, LAST_20_P, LAST_20_P_LEN);
	}
	
	// normal scan
	rewind(file);
	if (search_virus(file, virus, total_file_length) && !find)
	{
		find = FOUND;
		strncat(file_name_to_check, INFECTED_VIRUS, INFECTED_VIRUS_LEN);
	}

	if(!find)
	{
		strncat(file_name_to_check, CLEAN, CLEAN_LEN);
	}
	

	fputs(file_name_to_check, flog);
	fputc(NEW_LINE, flog);
	puts(file_name_to_check);

	fclose(file);
	fclose(virus);
}


/*  function get log file, and data, and put the data by the format in the log file
	inupt: log file + data -> strings
	output: none*/
void write_f_log(FILE* flog, char* dir_name, char* f_name, char* mode)
{
	// i dosnt fined a way to format the string
	fputs(F_LOG_DESCRIBE_1, flog);
	fputs(dir_name, flog);
	fputs(F_LOG_DESCRIBE_2, flog);
	fputs(f_name, flog);
	fputs(F_LOG_DESCRIBE_3, flog);
	fputs(mode, flog);
	fputs(F_LOG_DESCRIBE_4, flog);
}


/*  function get files path, and write to the log file if the file infected or not
	input: virus & file -> 2 strings path, flog -> log doccument file
	output: none  */
void file_contains_virus(char* virus_file_name, char* file_name_to_check, FILE* flog)
{
	FILE* file = NULL;
	FILE* virus = NULL;

	open_file(&file, file_name_to_check, READ_MODE);
	open_file(&virus, virus_file_name, READ_MODE);

	if (!valid_file(virus))
	{
		fclose(file);
		exit(ERROR_STATUS);
	}


	if (search_virus(file, virus, get_file_length(file)))
	{
		strncat(file_name_to_check, INFECTED_VIRUS, INFECTED_VIRUS_LEN);
	}
	else
	{
		strncat(file_name_to_check, CLEAN, CLEAN_LEN);
	}

	fputs(file_name_to_check, flog);
	fputc(NEW_LINE, flog);
	puts(file_name_to_check);


	fclose(file);
	fclose(virus);
}



/*  function get mode, and return the describe of the mode -> Normal/Quick Scan
	input: mode -> a user mode choice
	output: Normal/Quick Scan   */
char* get_mode(const char mode)
{
	if (mode == NORMAL)
	{
		return NORMAL_MODE;
	}
	return QUICK_SCAN;
}



/*  Use to open file, if failed program stop
	input: file ptr, path & mode to open
	output: none  */
void open_file(FILE** f, char* path, char* mode)
{
	*f = fopen(path, mode);
	if (!*(f))
	{
		printf("Error!\n\n");
		exit(ERROR_STATUS);
	}
}


/*  function get file and check if is ok, use if i opened files before, and if failed, i want to close them before exit
	input: file ptr to check
	output: true -> sucess, false -> error*/
int valid_file(FILE* f)
{
	int return_val = !f ? FAILED: SECESS;
	return return_val;
}


/*  The function get 2 files, and check if "file" contains "virus"
	input: file, virus -> files to check, bytes -> number of bytes to scan in "file"
	output: 1 -> contains, else -> 0  */
int search_virus(FILE* file, FILE* virus, long bytes)
{
	long current = 0, i = 0, index = 0;
	long virus_len = 0;
	int return_value = THE_FILE_IS_OK;
	int search = KEEP_SEARCHING;
	char* file_text = NULL;
	char* virus_text = NULL;
	// a string to compare with the virus
	char* compare_file = NULL;
	
	virus_len = get_file_length(virus);

	// get the start index (for the quick scan)
	i = ftell(file);

	//put the files data in strings
	file_text = (char*)malloc(get_file_length(file) + 1);
	virus_text = (char*)malloc(virus_len + 1);
	compare_file = (char*)malloc(virus_len + 1);

	fread(file_text, sizeof(char), get_file_length(file), file);
	fread(virus_text, sizeof(char), strlen(virus_text), virus);

	//close the strings
	file_text[get_file_length(file)] = '\0';
	virus_text[virus_len] = '\0';
	compare_file[virus_len] = '\0';
	
	if (get_file_length(file) < get_file_length(virus))
	{
		search = STOP_SEARCHING;
	}

	/*
	Algorithm Explaind:
		run through all the file, in any charactare start to check if he is the start
		of the virus -> compare the virus with the rest of the characters till the and of the virus len
		if they similar to the virus, we found the virus.
	In Quick Scan:
		the i get the current cursor and run from him to the maximum limit (bytes)
		scan start: i = 0, limit: file len * 20%
		scan end: i = file_len * 80%, limit 100%
		normal: i = 0, limit = file len * 100%
	*/
	for (i = i; (i < bytes) && search; ++i)
	{
		current = i;
		index = 0;
		while ((i < bytes) && (index < virus_len))
		{
			compare_file[index++] = file_text[i++];
		}

		if (strcmp(compare_file, virus_text) == EQUAL)
		{
			search = STOP_SEARCHING;
			return_value = FIND_THE_VIRUS;
		}
		i = current;
	}
	
	free(file_text);
	free(virus_text);
	free(compare_file);
	return return_value;
}


/*  The function get the the number of arguments, and check if it the expected number
	input: n -> number of arguments
	output: none  */
void valid_number_of_arguments(int n)
{
	if (n != PARAMS)
	{
		printf("Invalid number of argumemnts!\n");
		printf("Template: <file.exe> <directory> <virus file>\n\n");
		exit(ERROR_STATUS);
	}
}


/*  The function get a choice ptr, and ask the user for a scan option
	input: choice -> ptr, represente the choice of the user
	output: none   */
void get_scan_option(char* ch)
{
	printf("Press 0 for a normal scan or any other key for a quick scan: ");
	*(ch) = getchar();
	printf("\nScanning began...\n");
	printf("This process may take several minutes...\n\nScanning:\n");
}


/*  Credits: king Ras kissos
    Credits: https://stackoverflow.com/questions/4553012/checking-if-a-file-is-a-directory-or-just-a-file
    The function get path and check if he a directory
	input: path to check
	output: none    */
void is_directory(const char* path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	if (!S_ISDIR(path_stat.st_mode))
	{
		printf("%s is not a directory !\n\n", path);
		exit(1);
	}
}


/*  The function get a file, and calculate his length
	input: file -> file to check len
	output: len of the file  */
long get_file_length(FILE* file)
{
	long tmp = 0;
	fseek(file, 0, SEEK_END);
	tmp = ftell(file);
	rewind(file);
	return tmp;
}


/*  The function get a directory and a virus, and print a msg that we will start scanning the files in the folder
	input: directory -> to print the directory who scanned, virus_name -> a virus file name
	output: none*/
void wellcome_message(char* directory_name, char* virus_name)
{
	printf("Welcome to my Virus Scan!\n\n");
	printf("Folder to scan: %s\n", directory_name);
	printf("Virus signature: %s\n\n", virus_name);
}


/*  Function try to open dir
	input: dir ptr, path to the dir
	output: none*/
void open_dir(DIR** d, char* path)
{
	*(d) = opendir(path);

	if (!*(d))
	{
		printf("Error!\n");
		exit(ERROR_STATUS);
	}
}