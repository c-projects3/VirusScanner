#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define NUM_OF_ARGUMENTS 4

#define FILE_PATH 1
#define START_INDEX 2
#define END_INDEX 3

#define OPEN_SUCESS 1
#define OPEN_FAILED 0
#define SIGN_NAME "KittenVirusSign"

#define EXIT printf("Error: cant open file\n");exit(1);


/*  function get the expected number of arguments and check if it ok
	input: number of args
	output: none*/
void valid_number_of_arguments(int number)
{
	if (number != NUM_OF_ARGUMENTS)
	{
		printf("Invalid number of arguments!\n");
		printf("Template: <file.exe> <filepath> <start> <end>\n\n");
		getchar();
		exit(1);
	}
}

/*  function get file and check if he opened
	input: file to check
	output: none*/
int file_opened(FILE* f)
{
	if (!f)
	{
		return OPEN_FAILED;
	}
	return OPEN_SUCESS;
}

/*  function get file to copy form, new ptr file to write, start index and and index
	input: files and ofset
	output: none*/
void copy_file(FILE* to_cpy, FILE* to_write, int start, int end)
{
	char* ch = NULL;
	int ofset = 0;

	ofset = end - start + 1;
	ch = (char*)malloc(ofset);
	fseek(to_cpy, start, SEEK_SET);
	fread(ch, sizeof(char), ofset, to_cpy);
	fwrite(ch, sizeof(char), ofset, to_write);
	free(ch);
}


int main(int argc, char* argv[])
{
	FILE* f_1 = NULL;
	FILE* f_2 = NULL;
	
	valid_number_of_arguments(argc);

	f_1 = fopen(argv[FILE_PATH], "rb");
	if (!file_opened(f_1))
	{
		EXIT;
	}
	f_2 = fopen(SIGN_NAME, "wb");
	if (!file_opened(f_2))
	{
		fclose(f_1);
		EXIT;
	}

	copy_file(f_1, f_2, atoi(argv[START_INDEX]), atoi(argv[END_INDEX]));
	getchar();
	return 0;
}

